<?php
/**
 * Plugin Name:       Thumbnails
 * Plugin URI:        https://bitbucket.org/madebrave/thumbnail
 * Description:       Simple class for getting thumbnails in WP with a default.
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */


if ( !defined( 'ABSPATH' ) ) {
	die();
}


if(function_exists("get_field")) {
    require_once __DIR__.'/lib/Thumbnail.php';
} else {
    add_action('admin_notices', function() {
    	$class = 'notice notice-error';
    	$message = __('The Thumbnails plugin requires Advanced Custom Fields to be enabled.');

    	printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    });
}