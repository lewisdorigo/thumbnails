<?php namespace Dorigo;

class Thumbnail {
    private static $filters;
    private $thumbnail;

    private function addFilters() {
        add_filter("Dorigo/Thumbnails", [$this, 'defaultImage'], 10, 2);
        self::$filters = true;
    }

    public function __construct($post_id = null) {
        $post = get_post($post_id);

        if(is_null(self::$filters)) {
            $this->addFilters();
        }

        $image = apply_filters("Dorigo/Thumbnails/post_type={$post->post_type}", null, $post);
        $this->thumbnail = apply_filters("Dorigo/Thumbnails", $image, $post);
    }

    public function defaultImage($image, $post) {
        if(!$image) {

            if(has_post_thumbnail($post->ID)) {

        		$image = get_post_thumbnail_id($post->ID);
                $image = acf_get_attachment($image);

        	} else {

            	$image = get_field("thumbnail-{$post->post_type}", 'options');
            	$image = $image ?: get_field('thumbnail','options');
            }
        }

        return $image;
    }

    public function get(string $type = null) {
        if(!is_null($type)) {
            return isset($this->thumbnail[$type]) ? $this->thumbnail[$type] : null;
        }

        return $this->thumbnail;
    }
}
